# Executes commands at the start of an interactive session.

# For Ctrl-s remapping
# stty -ixon -ixoff

# Source Prezto
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
  fi

# OSX VPN Script
source $HOME/dotfiles/bash/vpn.bash

# virtualenvwrapper - http://www.doughellmann.com/docs/virtualenvwrapper/
source /usr/local/bin/virtualenvwrapper.sh

# OPAM configuration
. /Users/joe/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# Make Ctrl-P the same as up-arrow
bindkey "^P" up-line-or-search
bindkey "^N" down-line-or-search

# Aliases
alias tmux='tmux -2'
source $HOME/dotfiles/zsh/aliases.zsh
