#!/bin/bash
############################
# setup_dotfiles.sh
# dotfile setup script
# Based on https://github.com/michaeljsmalley/dotfiles/makesymlinks.sh
############################

########## Variables

dir=~/dotfiles                                           # dotfiles directory
olddir=~/old_dotfiles                                    # old dotfiles backup directory
files="vimrc vim gitconfig gitignore tmux.conf"          # list of files/folders to symlink in homedir
files="$files zshrc zshenv zpreztorc zlogin zlogout zprofile"
scripts="ack"                                               # list of scripts to symnlink to ~/bin

##########

# Platform Specific install
platform=$(uname);
if [[ $platform == 'Linux' ]]; then
        echo "Installing necessary packages."
        sudo apt-get install aptitude
        sudo aptitude install zsh python-pip mercurial git tmux build-essential
        sudo pip install virtualenv virtualenvwrapper
        echo "Installing neobundle.."
        mkdir -p ~/dotfiles/vim/bundle/
        cd ~/dotfiles/vim/bundle/
        git clone https://github.com/Shougo/neobundle.vim.git
        cd
        scripts="$scripts pbcopy pbpaste"                # linux only scripts

        echo "Installing vim from source"
        mkdir ~/src
        cd ~/src
        hg clone https://code.google.com/p/vim/
        cd vim
        ./configure --with-features=huge \
                    --enable-rubyinterp \
                    --enable-pythoninterp \
                    --with-python-config-dir=/usr/lib/python2.7/config \
                    --enable-perlinterp \
                    --enable-luainterp \
                    --enable-gui=gtk2 --enable-cscope --prefix=/usr
        make VIMRUNTIMEDIR=/usr/share/vim/vim74
        sudo make install
        sudo update-alternatives --install /usr/bin/editor editor /usr/bin/vim 1
        sudo update-alternatives --set editor /usr/bin/vim



elif [[ $platform == 'Darwin' ]]; then
        echo "Installing homebrew"
        ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"
        brew install zsh reattach-to-user-namespace wget
fi

# Install Prezto
git clone https://bitbucket.org/joeleong/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
chsh -s $(which zsh)

# Install Oh-My-Zsh
# echo "Installing Oh-My-Zsh"
# $dir/scripts/zsh_install.sh

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks from the homedir to any files in the ~/dotfiles directory specified in $files
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file $olddir
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
    if [[ $platform == 'Linux' ]]; then
        rm ~/.tmux.conf
        ln -s $dir/linux-tmux.conf ~/.tmux.conf
    fi
done

# mdir ~/bin if it doesn't alread exist
if [[ ! -d ~/bin ]]; then
        mkdir ~/bin
fi
# symlink the scripts
for script in $scripts; do
    echo "Symlinking scripts into ~/bin"
    ln -s $dir/scripts/$script ~/bin/
done

if [[ $platform == 'Linux' ]]; then
  cd ~/.vim/bundle/YouCompleteMe
  echo "After you install the vim plugins you should probably run: ./install.sh --clang-completer "
fi
