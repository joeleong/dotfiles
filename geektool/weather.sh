#!/bin/bash

curl --silent "http://weather.yahoo.com/united-states/missouri/st.-louis-2486982/" | grep "current-weather" | sed "s/.*background\:url(\'\(.*\)\') .*/\1/" | xargs curl --silent -o /tmp/weather1.png

curl --silent "http://weather.yahooapis.com/forecastrss?w=2486982&u=f" | grep -E '(Current Conditions:|F<BR)' | sed -e 's/Current Conditions://' -e 's/<br \/>//' -e 's/<b>//' -e 's/<\/b>//' -e 's/<BR \/>//' -e 's/<description>//' -e 's/<\/description>//'
