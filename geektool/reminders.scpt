set theList to {}
set theOutput to ""
--osascript ~/dotfiles/geektool/reminders.scpt
tell application "Reminders"
    repeat with i from 1 to (count of every reminder of list "Reminders")
        if reminder i of list "Reminders" is not completed then
            set theList to theList & name of reminder i of list "Reminders"
        end if
    end repeat
    repeat with i from 1 to (count of every item of theList)
        set theOutput to (theOutput & "-" & item i of theList as string) & linefeed & "" & linefeed
    end repeat
    return theOutput
end tell
