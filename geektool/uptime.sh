#!/bin/bash

echo "Uptime: $(uptime | awk '{sub(/[0-9]|user,|users,|load/, "", $6); sub(/mins,|min,/, "min", $6); sub(",", "min", $5); sub(":", "h ", $5); sub(/mins,|min,/, "min", $4); ; sub(" h ", " h ", $4); sub(/days,/, "days ", $4); sub(/days,/, " days ", $4); sub(":", " hours and ", $3); sub(",", " minutes", $3); print $3 $4 $5 $6 }' | sed -e 's/1days/1day /g' -Ee 's/[0-9][1-9]?user[s]?min//g')"
