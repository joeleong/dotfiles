#!/bin/bash

# echo $(top -l 2 | awk '/CPU usage/ && NR > 5 {print "CPU Usage:", $3, "User", $5, "Sys", $7, "Idle"}')''

top -l 2 | awk '

/CPU usage/ {USER = $3} 
/CPU usage/ {SYS = $5}
/CPU usage/ {IDLE = $7}

END {
printf "CPU Usage: %.2f%% Total, %.2f%% User, %.2f%% Sys, %.2f%% Idle\n", USER+SYS, USER, SYS, IDLE
}'

