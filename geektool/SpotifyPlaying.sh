#!/bin/bash

DATA=$(osascript -e 'tell application "System Events"
    set myList to (name of every process)
end tell
if myList contains "Spotify" then
    tell application "Spotify"
    if player state is stopped then
    set output to "Stopped"
    else
    set trackname to name of current track
    set artistname to artist of current track
    set albumname to album of current track
    if player state is playing then
    set output to trackname & "new_lineby " & artistname & "new_lineon " & albumname & "new_line" & "Playing on Spotify"
    else if player state is paused then
    set output to trackname & "new_line" & artistname & "new_line" & albumname & "new_line" & "Paused"
    end if
    end if
    end tell
else
    set output to "Spotify is not running"
end if')

if [[ "$DATA" == *"Spotify is not running"* ]]
then
        cp ~/Pictures/Spotify\ Artwork/noart.tiff ~/Pictures/Spotify\ Artwork/albumArt.tiff
else
        echo $DATA | awk -F new_line '{print $1}'
        echo $DATA | awk -F new_line '{print $2}'
        echo $DATA | awk -F new_line '{print $3}'
        echo $DATA | awk -F new_line '{print $4}'
        zsh -c "osascript ~/dotfiles/geektool/Spotify-Artwork1.scpt"
fi
exit 0
