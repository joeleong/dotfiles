#! /bin/bash
xml=$(curl http://www.reddit.com/.rss) 


n=$1
m=$((2*n))


echo $xml | sed -e 's:<item>:œ:g' -e 's:</item>:œ:g' | cut -d 'œ' -f$m | sed 's/.*<title>//' | sed 's:</title>.*::' | cut -d 'œ' -f1 | head -n 1

url=$(echo $xml | sed -e 's:<item>:œ:g' -e 's:</item>:œ:g' | cut -d 'œ' -f$m | cut -d '"' -f6)

##path to image location
curl $url -o /tmp/$1.jpg

link=$(echo $xml | sed -e 's:<item>:œ:g' -e 's:</item>:œ:g' | cut -d 'œ' -f$m | sed 's/.*<link>//' | sed 's:</link>.*::' | cut -d 'œ' -f1 |  head -n 1)

##There should be an easier way to deal with these if statements

if [ $n == "1" ]
then

##This path needs to be go to the  .webloc template file
cat ~/dotfiles/geektool/template.webloc | sed s{'!FLAG!'{$link{ > ~/Desktop/\ .webloc

##Another Path
SetFileIcon -image /tmp/1.jpg -file ~/Desktop/\ .webloc
fi

if [ $n == "2" ]
then
cat ~/dotfiles/geektool/template.webloc | sed s{'!FLAG!'{$link{ > ~/Desktop/\ \ .webloc
SetFileIcon -image /tmp/2.jpg -file ~/Desktop/\ \ .webloc
fi

if [ $n == "3" ]
then
cat ~/dotfiles/geektool/template.webloc | sed s{'!FLAG!'{$link{ > ~/Desktop/\ \ \ .webloc
SetFileIcon -image /tmp/3.jpg -file ~/Desktop/\ \ \ .webloc
fi
