#!/bin/bash 

echo "External IP: $(curl --silent http://checkip.dyndns.org | awk '{print $6}' | cut -f 1 -d '<')  Internal IP: $(ifconfig | grep -A 3 "en1:" | grep "inet " | cut -d " " -f2)"
