#!/bin/sh

total_memory_in_mb=$(sysctl -n hw.memsize | awk '{print $0/1048576}')

/usr/bin/vm_stat | sed 's/\.//' | awk '
/free/ {FREE_BLOCKS = $3}
/inactive/ {INACTIVE_BLOCKS = $3}
/speculative/ {SPECULATIVE_BLOCKS = $3}
/wired/ {WIRED_BLOCKS = $4} 

END {
# printf "Free: %.2f\n", ((FREE_BLOCKS+SPECULATIVE_BLOCKS)*4096/1048576)
# printf "Inactive: %.2f\n", ((INACTIVE_BLOCKS)*4096/1048576)
# printf "Total Free: %.2f\n", ((FREE_BLOCKS+SPECULATIVE_BLOCKS+INACTIVE_BLOCKS)*4096/1048576)
# printf "Wired: %.2f\n", ((WIRED_BLOCKS)*4096/1048576)
# printf "Active: %.2f\n", ('$total_memory_in_mb'-((FREE_BLOCKS+SPECULATIVE_BLOCKS+INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576))
# printf "Total Used: %.2f\n", (((INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576)+('$total_memory_in_mb'-((FREE_BLOCKS+SPECULATIVE_BLOCKS+INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576)))

printf "Memory Usage: %.2f%% Total, ", (((INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576)+('$total_memory_in_mb'-((FREE_BLOCKS+SPECULATIVE_BLOCKS+INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576)))*100/'$total_memory_in_mb'
printf "%.2f GB Used, ", (((INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576)+('$total_memory_in_mb'-((FREE_BLOCKS+SPECULATIVE_BLOCKS+INACTIVE_BLOCKS+WIRED_BLOCKS)*4096/1048576)))/1024
printf "%.2f GB Free\n", ((FREE_BLOCKS+SPECULATIVE_BLOCKS)*4096/1048576)/1024
}'
