from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.dgroups import simple_key_binder

def add_group_callback(group_name):
    qtile.addgroup(group_name)

def add_group(qtile):
    prompt = qtile.widgetMap["prompt"]
    prompt.startInput("New Group: ", add_group_callback)


# Modifiers
alt = 'mod1'
sup = 'mod4'
control = 'control'
shift = 'shift'

keys = [
    # Switch between windows in current stack pane
    Key( [sup], "k", lazy.layout.down()),
    Key( [sup], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key( [sup, shift], "j", lazy.layout.shuffle_down()),
    Key( [sup, shift], "k", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key( [sup], "l", lazy.layout.next()),
    Key( [sup], "h", lazy.layout.previous()),

    # Alt-Tab
    Key( [sup], "Tab",lazy.layout.next()),
    Key( [sup, shift], "Tab",lazy.layout.previous()),
    
    # Swap panes of split stack
    Key( [sup], "o", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with multiple stack panes
    Key( [alt, shift], "Return", lazy.layout.toggle_split()),
    Key( [sup, shift], "Return", lazy.layout.flip()),

    # Tab through different layouts
    Key( [alt], "Tab", lazy.nextlayout()),
    Key( [alt, shift], "Tab", lazy.prevlayout()),

    # Close window
    Key( [sup, shift], "w", lazy.window.kill()),

    # Volume Control
    Key( [control, alt], "equal", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key( [control, alt], "minus", lazy.spawn("amixer -c 0 -q set Master 2dB-")),

    # Programs
    Key( [sup], "Return", lazy.spawn("xterm")),
    Key( [sup], 'n', lazy.spawn("chromium-browser")),

    # Refresh and Shutdown
    Key( [sup, shift], "r", lazy.restart()),
    Key( [sup, shift], "q", lazy.shutdown()),

    # Launch command
    Key( [alt], "F1", lazy.spawncmd(prompt='$ ')),
    Key( [control], "space", lazy.spawncmd(prompt='$ ')),

    # Add New Group
    Key( [control, alt], "g", lazy.function(add_group)),

    # Qtile commander
    Key( [alt], "F2", lazy.qtilecmd(prompt='qsh: ')),

]

# Drag floating layouts.
mouse = [
    Drag([alt], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([alt], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([alt], "Button2", lazy.window.bring_to_front())
]

groups = [
    Group("1"),
    Group("2"),
    Group("3"),
]
for i in groups:
    # mod1 + letter of group = switch to group
    keys.append(
        Key( [sup], i.name, lazy.group[i.name].toscreen())
    )

    # mod1 + shift + letter of group = switch to & move focused window to group
    keys.append(
        Key( [control, sup], i.name, lazy.window.togroup(i.name))
    )

dgroups_key_binder = None

border_args = {
    'border_width': 1,
}

layouts = [
    layout.Max(),
    layout.MonadTall(),
    layout.Stack(stacks=2),
    layout.TreeTab(),
    layout.Tile(),
]

screens = [
    Screen(
        top = bar.Bar(
            [
                widget.WindowName(),
                # widget.Volume(),
                widget.Systray(),
                widget.Clock('%a %b %d  %I:%M %p'),
            ],
            20
        ),
        bottom = bar.Bar(
            [
                widget.GroupBox(fontsize=10),
                widget.Prompt(fontsize=15),
                widget.Spacer(),
                widget.CurrentLayout(fontsize=15),
            ],
            26
        ),
    ),
]

main = None
follow_mouse_focus = True
cursor_warp = False
floating_layout = layout.Floating(auto_float_types=[
    "notification",
    "toolbar",
    "splash",
    "dialog",
])
auto_fullscreen = True
widget_defaults = {
    'font': 'Terminus',
    'fontsize': 15,
    'padding': 3
}
