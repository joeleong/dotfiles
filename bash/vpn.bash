function vpn-connect {
DEFAULT_CONFIG="UIUC VPN"
CONFIG=${1:-$DEFAULT_CONFIG}
echo "Connecting $CONFIG"
$(/usr/bin/env osascript <<-EOF
tell application "System Events"
        tell current location of network preferences
                set VPN to service "$CONFIG"
                if exists VPN then connect VPN
                repeat while (current configuration of VPN is not connected)
                    delay 1
                end repeat
        end tell
end tell
EOF) 
}

function vpn-disconnect {
DEFAULT_CONFIG="UIUC VPN"
CONFIG=${1:-$DEFAULT_CONFIG}
echo "Disconnecting $CONFIG"
RETVAL=$(/usr/bin/env osascript <<-EOF
tell application "System Events"
        tell current location of network preferences
                set VPN to service "$CONFIG"
                if exists VPN then disconnect VPN
        end tell
end tell
EOF)
}
