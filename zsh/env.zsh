# =====================
# Environment Settings
# =====================

# Explicit PATH
export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin
export PATH=/usr/local/share/npm/bin:$PATH # npm
export PATH=/usr/texbin:$PATH # LaTeX
export PATH=/usr/local/bin:$PATH # Homebre
export PATH=~/bin:$PATH # Personal

# EDITOR
export EDITOR='vim'

# 256 color
export TERM='screen-256color'

# RVM
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
if [[ -s $HOME/.rvm/scripts/rvm ]] ; then source $HOME/.rvm/scripts/rvm ; fi

# Tmuxinator
[[ -s $HOME/.tmuxinator/scripts/tmuxinator ]] && source $HOME/.tmuxinator/scripts/tmuxinator
[[ -s $HOME/.tmuxinator/scripts/tmuxinator_completion ]] && source $HOME/.tmuxinator/scripts/tmuxinator

# Android SDK
export ANDROID_SDK_ROOT=/usr/local/Cellar/android-sdk/r20.0.1
